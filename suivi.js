/**
 * Created by duques_g on 3/8/16.
 */
var slack = require('slack');
var request = require('request');
var regexp = require('node-regexp')

var token = process.env.TOKEN_SLACK;

var intrapath = "https://intra.epitech.eu/" +  process.env.TOKEN_TEK + "/";
var suivimars = intrapath + "module/2015/M-GPR-680/FR-7-1/acti-212468/rdv/?format=json";

var p = new RegExp(/quentin\b/);

var open_slot = " \n";

setInterval(function() {
    console.log("Hello")
    request(suivimars, function (err, response, html) {
        var data = JSON.parse(html);

        for (var i = 0; i != data.slots.length; i++) {
            if (data.slots[i].title.match(p)) {
                for (var j = 0; j != data.slots[i].slots.length; j++) {
                    if (data.slots[i].slots[j] && !data.slots[i].slots[j].id_team) {
                        var date = new Date(data.slots[i].slots[j].date);
                        //console.log(data.slots[i].slots[j].date)
                        open_slot += date.toString() + "\n";

                    }
                }

            }
            //console.log(data.slots[i].title)
        }
        if (open_slot == " \n") {
            open_slot = "The slots for the April follow-up are still not open."
        }
        slack.chat.postMessage({
            token: token,
            channel: "#testchan",
            as_user: false,
            username: "Suivi Mars | Quentin",
            text: open_slot
        }, function (data) {
            open_slot = " \n";
        });
        //console.log(data.slots[0])
    });
}, 1000 * 60 * 10);