/**
 * Created by duques_g on 3/6/16.
 */

var request = require('request');
var slack = require('slack');

slack.api.test({hello:'world'}, console.log)

var botname = "Perspective";
var intrapath = "https://intra.epitech.eu/" +  process.env.TOKEN_TEK + "/";
var userpath = intrapath + "user/";

var token = process.env.TOKEN_SLACK;
var bot = slack.rtm.client();

var  userJson;
var noteJson;

function getLastNote(login) {
    request(userpath + login + "/notes?format=json", function(err, response, html) {
        var info = JSON.parse(html);
        noteJson = info;
        var lastNotes = info.notes[0];
        console.log(info)
        for (var i = 0; i != info.notes.size; i++) {
            if (info.notes[i].date > lastNotes.date ){
                lastNotes = info.notes[i];
            }
        }
        return lastNotes;
    });
}

bot.started(function(payload) {
    //console.log('payload from rtm.start', payload)
});

bot.user_typing(function(msg) {
   console.log('several users typing', msg);
});

bot.listen({token: token})

bot.message(function (data) {
    console.log(data)
   var message = data.text;
    console.log(message);
    if (message.substring(0, 4) == "user") {
        //console.log(message.substring(5));
        console.log("starting request for: " + message.substring(5));
        request(userpath + message.substring(5) + "?format=json", function (error, response, html) {
            if (!error && response.statusCode == 200) {
                console.log("Request Accepted for: " + message.substring(5))
                var info = JSON.parse(html);
                userJson = info;
                var logtime = 0;
                if (info.nsstat)
                    logtime = info.nsstat.active;
                var user = {
                    login: info.login,
                    name: info.title,
                    city: info.groups[0].title,
                    studentyear: info.studentyear,
                    credits: info.credits,
                    gpa: info.gpa[0].gpa,
                    picture: info.picture,
                    logtime: logtime
                };
                request(userpath + info.login + "/notes?format=json", function(err, response, html) {
                    var infoNote = JSON.parse(html);
                    noteJson = infoNote;
                    var lastNote = [];
                    if (infoNote.notes.length > 5) {
                        for (var i = infoNote.notes.length - 1; i != infoNote.notes.length - 6; i--)
                            lastNote.push(infoNote.notes[i]);
                    }

                    console.log(lastNote.title + ": " + lastNote.final_note)
                    var header = "{" + user.city + ", tek" + info.studentyear + "}";
                    var logtime = "logtime: [" + user.logtime + "]";
                    var body = "[" + user.credits + "] [" + user.gpa + "] " + logtime + ": " + user.name;
                    var tabToprint = [];

                    for (var i = 0; i != lastNote.length; i++) {
                        tabToprint.push("\n" + lastNote[i].title + ": " + lastNote[i].final_note)

                    }
                    var second_line = "\n" + lastNote.title + ": " + lastNote.final_note;

                    var text = header + " " + body;
                    for (var i = 0; i != lastNote.length; i++) {
                        text += "\n" + lastNote[i].title + ": " + lastNote[i].final_note;
                    }
                    var attach_tmp =
                        [{
                            fallback: "student Picture",
                            image_url: user.picture,
                        }];
                    attach_tmp = JSON.stringify(attach_tmp);
                    slack.chat.postMessage({
                        token: token,
                        channel: data.channel,
                        attachments: attach_tmp,
                        as_user: false,
                        username: botname,
                        text: text,

                    }
                        , function(data) {
                            console.log(data);
                        });
                });

            }
        })
    }
});
